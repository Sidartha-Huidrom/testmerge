const mysql = require('mysql');
const databaseConfiguration = require('./secrets/databaseConfiguration');

const pool = mysql.createPool(
    databaseConfiguration
    );

module.exports = pool