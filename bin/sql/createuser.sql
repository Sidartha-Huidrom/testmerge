CREATE TABLE user(
    user_id             SERIAL PRIMARY KEY,
    username            VARCHAR(64),
    user_password       VARCHAR(64),
    user_address        VARCHAR(64),
    occupation          VARCHAR(64),
    mobile              VARCHAR(16)
);