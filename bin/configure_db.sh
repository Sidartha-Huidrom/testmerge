#!/bin/bash

MYSQLPASSWORD='node_password'
MYSQLUSER='node_user'

echo "Configuring Database"

mysql -u $MYSQLUSER -p$MYSQLPASSWORD < ./bin/sql/dropdb.sql
mysql -u $MYSQLUSER -p$MYSQLPASSWORD < ./bin/sql/createdb.sql
mysql -u $MYSQLUSER -p$MYSQLPASSWORD < ./bin/sql/usedb.sql

mysql -u $MYSQLUSER -p$MYSQLPASSWORD nodedb < ./bin/sql/createuser.sql

echo "Database Configured"