const express = require('express');
const app = express();
const cors = require('cors');
// const bodyParser = require('body-parser');

const loginRouter = require('./api/auth/login');
const registerRouter = require('./api/auth/register');

app.use(express.json());


app.use(cors({origin: 'http://localhost:1234'}));

//Routes
app.use('/login', loginRouter);
app.use('/register', registerRouter);

app.use((req, res, next) => {
    const error = new Error('Nots Found');
    error.status = 404;
    next(error);
})


app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        message: error.message
    });
    console.log('message ',error.message);
})

module.exports = app;