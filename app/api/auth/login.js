const {Router} = require('express');
const router = new Router();
const pool = require('../../../databasePool');


router.post('/',(req, res, next) => {

    const name = req.body.username;
    const password = req.body.password;

    checkUser({
        user_name:name,
        user_password: password
    })
    .then((status) => {
        if(status == 200){
            res.status(200).json({
                user: name,
                message: "login successfull"
            });
        }
        else{
            res.status(400).json({
                message: "Email and Password does not match"
            });
        }
    })
    .catch((error) => {
        res.status(500).json({
            Erorr: error,
            message: error
        });
    });

})


function checkUser({user_name, user_password}){

    const query = "SELECT * from user WHERE username = ? AND user_password = ?";
    const values = [user_name, user_password];

    return new Promise((resolve, reject) => {
        pool.query(query,values,
            (error, response) => {

                if(error) return reject(error);

                if(response.length > 0 ){
                    resolve(200);
                }
                else{
                    message = "Email and Password does not match";
                    resolve(400);
                }

            }
        )
    });

}


module.exports = router;