const {Router} = require('express');
const router = new Router();
const pool = require('../../../databasePool');

router.post('/',(req, res, next) => {

    const name  = req.body.username;
    const pass  = req.body.password;
    const add   = req.body.address;
    const occ   = req.body.occupation;
    const mob   = req.body.mobile;

    validateUsername({user_name: name})
    .then( (respon) => {
        
        if(respon == 200){

            storeUser( {
                user_name : name,
                user_pass : pass,
                user_add : add,
                user_occ : occ,
                user_mob : mob}
            ).then((u_id) => {
                res.status(200).json({
                    user_id: u_id,
                    message: "Successfully Registered"
                })
            })
            .catch((error) => {
                res.status(500).json({
                    message: error 
                })
            });
        }
        else{
            res.status(400).json({
            message: "Username Already Exist!" 
            })
        }

    })
    .catch((error) => {
        console.log("ERROR ",error);
        res.status(500).json({
        message: error 
        })
    });
    }
)


function storeUser( {user_name, user_pass, user_add, user_occ, user_mob }){
    
    let query = "INSERT INTO user (username, user_password, user_address, occupation, mobile) VALUES (?, ?, ?, ?, ?) ";
    let values = [user_name, user_pass, user_add, user_occ, user_mob];


    return new Promise((resolve, reject) => {
        pool.query(query,values,
            (error, response) => {

                if(error) return reject(error);
                
                const user_id = response.insertId;
                resolve(user_id);
            }
        )
    });

}

function validateUsername( {user_name}){
    
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * from user WHERE username = ?`, [user_name],
            (error, response) => {

                if(error) return reject(error);
            
                if(response.length < 1 ){
                    resolve(200);
                }
                else{
                    message = "Username Already Exist!";
                    resolve(400);
                }

            }
        )
    });
}

module.exports = router;